package model.service;

import config.Config;
import model.Person;
import model.contact.Contact;
import model.contact.ContactType;
import org.junit.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class TestObjectPhoneBook {

    @Test
    public void testAdd() {

        init();

        PhoneBookService phoneBook = new ObjectPhoneBook(Config.FILENAME_OBJECT_TEST);

        Person person;
        List<Contact> contacts = new ArrayList<>();

        assertEquals(0, phoneBook.sort().size());

        person = new Person("Иван", "Иванов");

        contacts.add(new Contact(ContactType.PHONE, "+380931234567"));
        contacts.add(new Contact(ContactType.EMAIL, "iiv@ukr.net"));

        person.setContacts(contacts);

        phoneBook.add(person);

        assertEquals(1, phoneBook.sort().size());

        contacts = new ArrayList<>();

        person = new Person("Сергей", "Антоненко");

        contacts.add(new Contact(ContactType.PHONE, "380671112111"));

        person.setContacts(contacts);

        phoneBook.add(person);

        assertEquals(2, phoneBook.sort().size());

        contacts = new ArrayList<>();

        person = new Person("Andrew", "Kim");

        contacts.add(new Contact(ContactType.EMAIL, "akim@gmail.com"));

        person.setContacts(contacts);

        phoneBook.add(person);

        assertEquals(3, phoneBook.sort().size());

    }

    @Test
    public void testSort() {

        init();

        PhoneBookService phoneBook = getNewPhoneBook();

        List<Person> sorted = phoneBook.sort();

        assertEquals("Andrew Kim", sorted.get(0).toString().split("\n")[1]);

        assertEquals("Иван Иванов", sorted.get(sorted.size() - 1).toString().split("\n")[1]);

    }

    @Test
    public void testFilter() {

        init();

        PhoneBookService phoneBook = getNewPhoneBook();

        List<Person> filtered = phoneBook.filter(ContactType.PHONE);

        assertEquals(2, filtered.size());

        filtered = phoneBook.filter(ContactType.EMAIL);

        assertEquals(2, filtered.size());

    }

    @Test
    public void testSearchByName() {

        init();

        PhoneBookService phoneBook = getNewPhoneBook();

        assertEquals(1, phoneBook.searchByName("и").size());
        assertEquals(1, phoneBook.searchByName("сергей").size());
        assertEquals(1, phoneBook.searchByName("Andrew").size());
        assertEquals(0, phoneBook.searchByName("Андрей").size());

    }

    @Test
    public void testSearchByContact() {

        init();

        PhoneBookService phoneBook = getNewPhoneBook();

        assertEquals(1, phoneBook.searchByContact("+").size());
        assertEquals(1, phoneBook.searchByContact("380").size());
        assertEquals(0, phoneBook.searchByContact("38096").size());
        assertEquals(1, phoneBook.searchByContact("a").size());
        assertEquals(1, phoneBook.searchByContact("i").size());
        assertEquals(0, phoneBook.searchByContact("x").size());

    }

    @Test
    public void testDelete() {

        init();

        PhoneBookService phoneBook = getNewPhoneBook();

        phoneBook.delete("akim@gmail.com");

        assertEquals(2, phoneBook.sort().size());

        phoneBook.delete("iiv@gmail.com");

        assertEquals(2, phoneBook.sort().size());

        phoneBook.delete("+380931234567");

        assertEquals(1, phoneBook.sort().size());

        phoneBook.delete("380671112111");

        assertEquals(0, phoneBook.sort().size());

        phoneBook.delete("380671112111");

        assertEquals(0, phoneBook.sort().size());

    }

    @Test
    public void testExists() {

        init();

        PhoneBookService phoneBook = getNewPhoneBook();

        assertTrue(phoneBook.exists(new Contact(ContactType.PHONE, "+380931234567")));
        assertTrue(phoneBook.exists(new Contact(ContactType.PHONE, "380671112111")));

        assertFalse(phoneBook.exists(new Contact(ContactType.PHONE, "+380681112111")));

        assertTrue(phoneBook.exists(new Contact(ContactType.EMAIL, "iiv@ukr.net")));
        assertTrue(phoneBook.exists(new Contact(ContactType.EMAIL, "akim@gmail.com")));

        assertFalse(phoneBook.exists(new Contact(ContactType.EMAIL, "iiv@gmail.com")));

        assertFalse(phoneBook.exists(new Contact(ContactType.EMAIL, "380671112111")));

        assertFalse(phoneBook.exists(new Contact(ContactType.PHONE, "iiv@ukr.net")));

    }

    private static void init() {
        try(FileWriter writer = new FileWriter(Config.FILENAME_OBJECT_TEST)) {} catch (IOException exception) {}
    }

    private static PhoneBookService getNewPhoneBook() {

        PhoneBookService phoneBook = new ObjectPhoneBook(Config.FILENAME_OBJECT_TEST);

        Person person;
        List<Contact> contacts = new ArrayList<>();

        person = new Person("Иван", "Иванов");

        contacts.add(new Contact(ContactType.PHONE, "+380931234567"));
        contacts.add(new Contact(ContactType.EMAIL, "iiv@ukr.net"));

        person.setContacts(contacts);

        phoneBook.add(person);

        contacts = new ArrayList<>();

        person = new Person("Сергей", "Антоненко");

        contacts.add(new Contact(ContactType.PHONE, "380671112111"));

        person.setContacts(contacts);

        phoneBook.add(person);

        contacts = new ArrayList<>();

        person = new Person("Andrew", "Kim");

        contacts.add(new Contact(ContactType.EMAIL, "akim@gmail.com"));

        person.setContacts(contacts);

        phoneBook.add(person);

        return phoneBook;

    }

}
