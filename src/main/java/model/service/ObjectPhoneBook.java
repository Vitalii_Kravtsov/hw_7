package model.service;

import lombok.RequiredArgsConstructor;
import model.Person;
import model.contact.Contact;
import model.contact.ContactType;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@RequiredArgsConstructor
public class ObjectPhoneBook implements PhoneBookService {

    private final String filename;

    @Override
    public boolean add(Person person) {

        List<Person> persons;

        try(ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(this.filename))) {
            persons = (ArrayList<Person>) inputStream.readObject();
        } catch (EOFException exception) {
            persons = new ArrayList<>();
        } catch (IOException | ClassNotFoundException exception) {
            return false;
        }

        try(ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(this.filename))) {

            persons.add(person);

            outputStream.writeObject(persons);

            outputStream.flush();

            return true;

        } catch (IOException exception) {
            return false;
        }

    }

    @Override
    public List<Person> sort() {

        List<Person> persons = this.readObjects();

        if (persons == null)
            return null;

        return persons.stream().sorted().collect(Collectors.toList());

    }

    @Override
    public List<Person> filter(ContactType type) {

        List<Person> persons = this.readObjects();

        if (persons == null)
            return null;

        List<Person> filtered = new ArrayList<>();

        for (Person person : persons) {

            List<Contact> contacts = new ArrayList<>();

            for (Contact contact : person.getContacts())
                if (contact.getType().equals(type))
                    contacts.add(contact);

            if (contacts.size() != 0) {

                Person newPerson = new Person(person.getFirstName(), person.getLastName());

                newPerson.setContacts(contacts);

                filtered.add(newPerson);

            }

        }

        return filtered;

    }

    @Override
    public List<Person> searchByName(String pattern) {

        List<Person> persons = this.readObjects();

        if (persons == null)
            return null;

        return persons.stream().filter(
                person -> person.getFirstName().toLowerCase().contains(pattern.toLowerCase())).collect(Collectors.toList()
        );

    }

    @Override
    public List<Person> searchByContact(String pattern) {

        List<Person> persons = this.readObjects();

        if (persons == null)
            return null;

        List<Person> result = new ArrayList<>();

        for (Person person : persons) {

            for (Contact contact : person.getContacts())
                if (contact.getValue().startsWith(pattern)) {

                    result.add(person);

                    break;

                }

        }

        return result;

    }

    @Override
    public boolean delete(String value) {

        List<Person> persons = this.readObjects();

        if (persons == null)
            return false;

        Person toDelete = null;

        for (Person person : persons)
            for (Contact contact : person.getContacts())
                if (contact.getValue().equals(value)) {
                    toDelete = person;
                    break;
                }

        if (toDelete != null) {

            persons.remove(toDelete);

            try(ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(this.filename))) {
                outputStream.writeObject(persons);
                return true;
            } catch (IOException exception) {
                return false;
            }

        } else return false;

    }

    @Override
    public boolean exists(Contact contact) {

        List<Person> persons = this.readObjects();

        if (persons == null)
            return false;

        for (Person person : persons)
            for (Contact c : person.getContacts())
                if (c.equals(contact))
                    return true;

        return false;

    }

    private List<Person> readObjects() {

        List<Person> persons;

        try(ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(this.filename))) {

            persons = (ArrayList<Person>) inputStream.readObject();

            return persons;

        } catch (EOFException exception) {
            return new ArrayList<>();
        } catch (IOException | ClassNotFoundException exception) {
            exception.printStackTrace();
            return null;
        }

    }

}
