package model;

import lombok.RequiredArgsConstructor;
import lombok.Data;
import model.contact.Contact;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;


@Data
@RequiredArgsConstructor
public class Person implements Comparable<Person>, Serializable {

    private UUID uuid = UUID.randomUUID();

    private final String firstName;
    private final String lastName;

    private List<Contact> contacts;

    @Override
    public int compareTo(Person o) {

        int compareLastName = this.lastName.toLowerCase().compareTo(o.lastName.toLowerCase());

        return compareLastName != 0 ? compareLastName : this.firstName.toLowerCase().compareTo(o.firstName.toLowerCase());

    }

    @Override
    public String toString() {

        StringBuilder result = new StringBuilder(String.format("%s\n%s %s\n", this.uuid, this.firstName, this.lastName));

        if (this.contacts != null)
            for (Contact contact : this.contacts)
                result.append("\t").append(contact).append("\n");

        return result.toString();

    }

}
