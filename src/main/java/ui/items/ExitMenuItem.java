package ui.items;


public class ExitMenuItem implements MenuItem {

    @Override
    public boolean isFinal() {
        return true;
    }

    @Override
    public String getMessage() {
        return "Выход";
    }

    @Override
    public void run() {}

}
