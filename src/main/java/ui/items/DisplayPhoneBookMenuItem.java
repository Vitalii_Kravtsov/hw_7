package ui.items;

import lombok.RequiredArgsConstructor;
import model.Person;
import model.contact.ContactType;
import model.service.PhoneBookService;
import ui.Display;

import java.util.List;


@RequiredArgsConstructor
public class DisplayPhoneBookMenuItem implements MenuItem {

    private final Display display;
    private final PhoneBookService phoneBookService;

    @Override
    public boolean isFinal() {
        return false;
    }

    @Override
    public String getMessage() {
        return "Просмотреть контакты с сортировкой по фамилии";
    }

    @Override
    public void run() {

        List<Person> persons = this.phoneBookService.sort();

        if (persons != null)
            this.display.displayPersons(persons);
        else System.out.println("\nОшибка чтения контактов\n");

    }

}
