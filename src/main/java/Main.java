import config.Config;
import model.service.PhoneBookService;
import ui.Display;
import ui.Menu;
import ui.items.*;

import java.util.Scanner;


public class Main {

    private static final Scanner scanner = new Scanner(System.in);

    private static final Display display = new Display();
    private static final PhoneBookService phoneBook = Config.PHONE_BOOK;

    private static final MenuItem[] items = {
            new AddPersonMenuItem(scanner, display, phoneBook),
            new DisplayPhoneBookMenuItem(display, phoneBook),
            new DisplayPhonesMenuItem(display, phoneBook),
            new DisplayEmailsMenuItem(display, phoneBook),
            new SearchByNameMenuItem(scanner, display, phoneBook),
            new SearchByContactMenuItem(scanner, display, phoneBook),
            new DeleteContactMenuItem(scanner, phoneBook),
            new ExitMenuItem()
    };

    private static final Menu menu = new Menu(items, scanner);

    public static void main(String[] args) {
        menu.run();
    }

}
