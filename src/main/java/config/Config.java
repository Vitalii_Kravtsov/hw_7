package config;

import model.service.*;


public class Config {

    // public static final PhoneBookService PHONE_BOOK = new MemoryPhoneBook();
    // public static final PhoneBookService PHONE_BOOK = new CSVPhoneBook(Config.FILENAME_CSV);
    public static final PhoneBookService PHONE_BOOK = new ObjectPhoneBook(Config.FILENAME_OBJECT);

    public static final String FILENAME_CSV = "persons.csv";
    public static final String FILENAME_OBJECT = "persons.obj";

    public static final String FILENAME_CSV_TEST = "persons-test.csv";
    public static final String FILENAME_OBJECT_TEST = "persons-test.obj";

}
